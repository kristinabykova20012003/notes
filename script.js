const calendar = document.querySelector(".calendar"),
    date = document.querySelector(".date"),
    daysContainer = document.querySelector(".days"),
    prev = document.querySelector(".prev");
    next = document.querySelector(".next"),
    noteDay = document.querySelector(".note-day"),
    noteDate = document.querySelector(".note-date"),
    notesContainer = document.querySelector(".notes");
    addNotesSubmit = document.querySelector(".add-note-btn");

let today = new Date();
let activeDay;
let month = today.getMonth();
let year = today.getFullYear();

const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "Movember",
    "December"
];

const notesArr = [];
getNotes();
 
//функция добавления дней
function initCalendar(){
    //чтобы получить предыдущие дни месяца и все дни текущего месяца и возобновить дни следующего месяца
    const firstDay = new Date(year, month,1);
    const lastDay = new Date (year, month + 1, 0 );
    const prevLastDay = new Date(year, month, 0);
    const prevDays = prevLastDay.getDate();
    const lastDate = lastDay.getDate();
    const day = firstDay.getDay();
    const nextDays = 7 - lastDay.getDay() -1;
    //обновление даты календаря
    date.innerHTML = months[month] + " " + year;
    //добавление дней
    let days = "";
    //предыдущие дни месяца
    for (let x = day; x > 0; x--){
        days += `<div class ="day prev-date">${prevDays-x+1}</div>`;
    }
    //дни текущего месяца
    for (let i=1; i <= lastDate; i++){
        //проверка есть ли записи в текущий день
        let note = false;
        notesArr.forEach((noteObj) => {
            if(
                noteObj.day === i &&
                noteObj.month === month + 1 &&
                noteObj.year === year
            ){
                note=true;
            }
        });
        if (i === new Date().getDate() && year === new Date().getFullYear() && month === new Date().getMonth()){
            activeDay = i;
            getActiveDay(i);
            updateNotes(i);
            if(note){
                days += `<div class ="day today active note">${i}</div>`;
            }
            else {
                days += `<div class ="day today active">${i}</div>`;
            }
        }
        //оставшиеся дни
        else {
            if(note){
                days += `<div class ="day note">${i}</div>`;
            }
            else {
                days += `<div class ="day ">${i}</div>`;
            }
        }
    }
    //дни следующего месяца
    for (let j=1; j <= nextDays; j++){
        days += `<div class ="day next-date">${j}</div>`;
    }
    daysContainer.innerHTML = days;
    addListner();
}
initCalendar();
//предыдущий месяц 
function prevMonth() {
    month --;
    if (month < 0){
        month = 11;
        year --;
    }
    initCalendar();
}
// следующий месяц
function nextMonth(){
    month++;
    if(month > 11){
        month = 0;
        year++;
    }
    initCalendar();
}
prev.addEventListener("click", prevMonth);
next.addEventListener("click", nextMonth);

const addNoteBth = document.querySelector(".add-note"),
addNoteContainer = document.querySelector(".add-note-wrapper"),
addNoteCloseBth = document.querySelector(".close"),
addNoteTitle = document.querySelector(".title-note"),
addNoteDescription = document.querySelector(".description-note");

addNoteBth.addEventListener("click", () => {
    addNoteContainer.classList.toggle("active");
});
addNoteCloseBth.addEventListener("click", () => {
    addNoteContainer.classList.remove("active");
});

document.addEventListener("click", (e) => {
    if (e.target !== addNoteBth && !addNoteContainer.contains(e.target)){
        addNoteContainer.classList.remove("active");
    }
});

//функция добавления заметки
function addListner(){
    const days = document.querySelectorAll(".day");
    days.forEach((day)=> {
        day.addEventListener("click", (e) => {
            activeDay = Number(e.target.innerHTML);
            getActiveDay(e.target.innerHTML);
            updateNotes(Number(e.target.innerHTML));
            days.forEach((day) => {
                day.classList.remove("active");
              });
            if (e.target.classList.contains("prev-date")){
                prevMonth();
                setTimeout(() => {
                    const days = document.querySelectorAll(".day");
                    days.forEach((day) => {
                        if(!day.classList.contains("prev-date") && day.innerHTML === e.target.innerHTML ){
                            day.classList.add("active")
                        }
                    });
                }, 100);
            } else if (e.target.classList.contains("next-date")){
                nextMonth();
                setTimeout(() => {
                    const days = document.querySelectorAll(".day");
                    days.forEach((day) => {
                        if(!day.classList.contains("next-date") && day.innerHTML === e.target.innerHTML ){
                            day.classList.add("active")
                        }
                    });
                }, 100);
            }else {
                e.target.classList.add("active");
            }      
        });
    });
}

//отображения заметок и даты вверху
function getActiveDay(date){
    const day = new Date(year,month,date);
    const dayName = day.toString().split(" ")[0];
    noteDay.innerHTML = dayName;
    noteDate.innerHTML = date + " " + months[month] + " " + year;
}

//отображение заметок в конкретный день
function updateNotes(date){
    let notes = "";
    notesArr.forEach((note) =>{
        if (
            date === note.day &&
            month +1  === note.month &&
            year === note.year 
        ){
            note.notes.forEach((note)=> {
                notes += `
                <div class="note">
                    <div class="title">
                        <i class="fas fa-fg"></i>
                        <h3 class="note-title">${note.title}</h3>
                    </div>
                    <div class="description">
                        <span class="note-description">${note.description}</span>
                    </div>
                </div> `;
            });
        }
    });
    if (notes === ""){
        notes = `<div class="no-notes">
                <h3>No Notes</h3>
            </div> `;
    }
    notesContainer.innerHTML = notes;
    saveNotes();
}

//добавление новых заметок
addNotesSubmit.addEventListener("click", () => {
    const noteTitle = addNoteTitle.value;
    const noteDescription = addNoteDescription.value;
    if(noteTitle === "" || noteDescription === "" ){
        alert("Заполните все поля")
        return;
    }
    const newNote = {
        title: noteTitle,
        description: noteDescription,
    };
    let noteAdded = false;
    if(notesArr.length >0){
        notesArr.forEach((item) => {
            if (
                item.day === activeDay && item.month === month+1 && item.year === year
            ){
                item.notes.push(newNote);
                noteAdded = true;
            }
        })
    }
    if (!noteAdded){
        notesArr.push({
            day: activeDay,
            month: month+1,
            year: year,
            notes: [newNote]
        });
    }
    addNoteContainer.classList.remove("active")
    addNoteTitle.value = "";
    addNoteDescription.value = "";
    updateNotes(activeDay);
    const activeDayElem = document.querySelector(".day.active");
    if (!activeDayElem.classList.contains("note")){
        activeDayElem.classList.add("note");
    }
});

notesContainer.addEventListener("click", (e) => {
    if(e.target.classList.contains("note")){
        const noteTitle = e.target.children[0].children[1].innerHTML;
        notesArr.forEach((note) => {
            if(
                note.day === activeDay &&
                note.month === month+1 &&
                note.year === year
            ){
                note.notes.forEach((item, index) => {
                    if(item.title === noteTitle){
                        note.notes.splice(index,1);
                    }
                });
                if(note.notes.length === 0){
                    notesArr.splice(notesArr.indexOf(note),1);
                }
                const activeDayElem = document.querySelector(".day.active")
                if(activeDayElem.classList.contains("note")){
                    activeDayElem.classList.remove("note")
                }
            }
        });
        updateNotes(activeDay);
    }
});

function saveNotes() {
    localStorage.setItem("notes", JSON.stringify(notesArr));
  }
  function getNotes() {
    if (localStorage.getItem("notes") === null) {
      return;
    }
    notesArr.push(...JSON.parse(localStorage.getItem("notes")));
  }
  